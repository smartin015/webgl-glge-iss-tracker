function readoutFromSGP4(sat) {
    var lat_integer = Math.floor(sat.latitude);
    var long_integer = Math.floor(sat.longitude);
    if (lat_integer>0){
      var lat_decimal = Math.floor((sat.latitude-lat_integer)*60*100)/100;
      var lat = Math.abs(lat_integer)+"&deg; " + Math.abs(lat_decimal) + "&prime; N";
    }else{
      var lat_decimal = Math.floor(6000-(sat.latitude-lat_integer)*60*100)/100;
      var lat = Math.abs(lat_integer+1)+"&deg; " + Math.abs(lat_decimal) + "&prime; S";
    }
    if (long_integer>0){
      var long_decimal = Math.floor((sat.longitude-long_integer)*60*100)/100;
      var lon = Math.abs(long_integer)+"&deg; " + Math.abs(long_decimal) + "&prime; E";
    }else{
      var long_decimal = Math.floor(6000-(sat.longitude-long_integer)*60*100)/100;
      var lon = Math.abs(long_integer+1)+"&deg; " + Math.abs(long_decimal) + "&prime; W";
    }
    var v_unit = "mph";
    var d_unit = "sm";
    var u = 0.621371192;
    var alt = Math.floor(Number(sat.altitude)*u*1000)/1000 + " " + d_unit;
    var v = Math.floor(Number(sat.velocity)*u*1000*3600)/1000 + " " + v_unit;;
    return {"lat": lat,
            "lon": lon,
            "alt": alt,
            "v": v};
}
  
  
window.requestAnimFrame = (function() {
    //Taken from a google toolkit:
    //https://www.khronos.org/registry/webgl/sdk/demos/common/webgl-utils.js
    return window.requestAnimationFrame ||
        window.webkitRequestAnimationFrame ||
        window.mozRequestAnimationFrame ||
        window.oRequestAnimationFrame ||
        window.msRequestAnimationFrame ||
        function(/* function FrameRequestCallback */ callback, /* DOMElement Element */ element) {
          window.setTimeout(callback, 1000/60);
        };
})();