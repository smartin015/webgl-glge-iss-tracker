var TLEdecode = function(data) {
    //Shamelessly stolen from Google Sat Track
    //http://www.lizard-tail.com/isana/tracking/
    var dataLines = data.split("\n");
    var line1 = dataLines[1];
    var line2 = dataLines[2];
    var orbital_elements={
    line_number_1 :   Number(line1.slice(0,0)),
    catalog_no_1 :  Number(line1.slice(2,7)),
    security_classification :  Number(line1.slice(7,7)),
    international_identification : String(line1.slice(9,17)),
    epoch_year : Number(line1.slice(18,20)),
    epoch : Number(line1.substring(20,32)),
    first_derivative_mean_motion : Number(line1.substring(33,43)),
    second_derivative_mean_motion : Number(line1.substring(44,52)),
    bstar_mantissa: Number(line1.substring(53,59)),
    bstar_exponent : Number(line1.substring(59,61)),
    ephemeris_type :  Number(line1.substring(62,63)),
    element_number :  Number(line1.substring(64,68)),
    check_sum_1 :   Number(line1.substring(69,69)),
    line_number_2 :   Number(line1.slice(0,0)),
    catalog_no_2 :  Number(line2.slice(2,7)),
    inclination : Number(line2.substring(8,16)),
    right_ascension : Number(line2.substring(17,25)),
    eccentricity : Number(line2.substring(26,33)),
    argument_of_perigee : Number(line2.substring(34,42)),
    mean_anomaly : Number(line2.substring(43,51)),
    mean_motion : Number(line2.substring(52,63)),
    rev_number_at_epoch : Number(line2.substring(64,68)),
    check_sum_2 :   Number(line1.substring(68,69)) 
   }
   return orbital_elements;
}

var Satellite = function(TLE_text, obj, scene) {
    this.obj = obj;
    this.TLE = TLEdecode(TLE_text);
    this.SGP4 = new SGP4(this.TLE);
    this.xyz = [0,0,0];
    
    var lastUpdate = 0;
    var ISS_vel = [0,0,0];
    var target = [0,0,0];
    
    function isZeroVec3(vec){return (vec[0] == 0 && vec[1] == 0 && vec[2] == 0);}
    var currPos;
    var delta;
    var updateMesh = false;
    this.update = function(lasttime, now) {
        if (now - lastUpdate > 10000) {
            lastUpdate = now;
            updateMesh = !updateMesh;
            var vecIndex = (updateMesh) ? 3 : 0;
            currPos = this.path.slice(vecIndex,vecIndex+3);
            target = this.path.slice(vecIndex+3,vecIndex+6);

            delta = GLGE.subVec3(target, currPos);
            
            if (updateMesh) {
                this.path.splice(0,6);
                this._pushPath((PATHLEN-2)*PATH_PT_INTERVAL_MS);
                this._pushPath((PATHLEN-1)*PATH_PT_INTERVAL_MS);
                this.pathMesh.setPositions(this.path);
            }
        }
        
        var frac = (now - lastUpdate)/PATH_PT_INTERVAL_MS;
        this.xyz = GLGE.addVec3(GLGE.scaleVec3(delta, frac), currPos);
        this.obj.setLoc(this.xyz[0], this.xyz[1], this.xyz[2]);
    }
    
    this.predictPos = function(delta_ms) {
        if (delta_ms == null) delta_ms = 0;
        //Predict the lat/lon/etc of satellite delta_ms from now
        var date = new Date();
        date.setTime(date.getTime() + delta_ms)
        position = this.SGP4.calc(date);
        position.latlng();
        return position;
    }
    
    //Create a list of XYZ points representing the future
    //path of object with id.
    var PATHLEN = 600;
    var PATH_PT_INTERVAL_MS = 10000;
    this.path = [];
    this._pushPath = function(delta_ms) {
        var pos = this.predictPos(delta_ms);
        var xyz = GEOM.geo2XYZ(pos.latitude, pos.longitude, pos.altitude*GEOM.KM_TO_MI);
        this.path.push(xyz[0]); 
        this.path.push(xyz[1]); 
        this.path.push(xyz[2]);
    }
    for (var i = 0; i < PATHLEN; i++)
        this._pushPath(i*PATH_PT_INTERVAL_MS);
        
    var line = (new GLGE.Object).setDrawType(GLGE.DRAW_LINES);
    this.pathMesh = (new GLGE.Mesh).setPositions(this.path);
    line.setMesh(this.pathMesh);
    line.setMaterial(XMLdoc.getElement("lines"));
    scene.addObject(line);
    
    return this;
};