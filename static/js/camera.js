var Camera = function(webgl_object, views, canvas) {
    this.obj = webgl_object;
    
    this.views = views;
    this.viewIndex = 0;
    this.view = "";
    this.base_mag = 0;
    this.getLookTarget = function() {return [0,0,0]};
    
    this.mag = 20;
    this.right = 0;
    this.up = 0;
    this.xyz = [30,0,0];
    
    var VERT_SPEED = 1;
    var VERT_MIN = -Math.PI/2;
    var VERT_MAX = Math.PI/2;
    var HORIZ_SPEED = 1;
    var ALT_SPEED = 1.1;
    var CLOSING_FRACTION = 0.5/50; //Used in camera smoothing average
    
    this.setSpr = function(up, right, mag) {
        //Snap to spherical coordinates
        this.up = up;
        this.right = right;
        this.mag = mag;
        this.xyz = GLGE.addVec3(this.getLookTarget(), GEOM.spr2XYZ(this.up, this.right, this.mag + this.base_mag));
    }
    
    this.setView = function(i) {
        this.viewIndex = i;
        this.view = this.views[i][0];
        this.base_mag = this.views[i][1];
        this.getLookTarget = this.views[i][2];
    }
    this.setView(0);
    
    this.setLookTargetFunc = function(targetFunc) {
        this.getLookTarget = targetFunc;
    }
    
    this.update = function(lasttime, now) {    
        var target = GLGE.addVec3(this.getLookTarget(), GEOM.spr2XYZ(this.up, this.right, this.mag + this.base_mag));
        var delta = GLGE.subVec3(target, this.xyz);
        this.xyz = GLGE.addVec3(this.xyz, GLGE.scaleVec3(delta, CLOSING_FRACTION*(now-lasttime)));
        this.obj.setLoc(this.xyz[0], this.xyz[1], this.xyz[2]);
        this.obj.setRotMatrix(GEOM.lookAt(this.xyz, this.getLookTarget()));
    };
    
    this.obj.setNear(0.001);
    this.obj.setFar(Math.pow(2,32));
    this.obj.setFovY(29.863); //Just a guess. TODO: Refine this!
    
    //---------- Bind mouse events -------------
    var startpoint;
    var drag=false;
    var view=false;
    var cam = this;
    
    canvas.onmousedown=function(e){
        if(e.button==0){
            view=true;
            startpoint=[e.clientX,e.clientY,cam.right,cam.up];
        }
        e.preventDefault();
    }
    canvas.onmouseup=function(e){
        view=false;
    }
    canvas.onmousemove=function(e){
        if(view){
            cam.right = startpoint[2]-(e.clientX-startpoint[0])/canvas.width*HORIZ_SPEED;
            cam.up = startpoint[3]+(e.clientY-startpoint[1])/canvas.height*VERT_SPEED;
            cam.up = Math.max(Math.min(cam.up, VERT_MAX), VERT_MIN);
        }
    }
    canvas.onmousewheel=function(e){
        var wheelData = e.detail ? e.detail/10 : e.wheelDelta/-300;
        cam.mag *= Math.pow(ALT_SPEED,wheelData);
    };
    canvas.addEventListener('DOMMouseScroll', canvas.onmousewheel, false);
    
    $(window).keypress(function(e) {
        if (e.keyCode == 32){ //Spacebar
            cam.setView((cam.viewIndex + 1) % cam.views.length);            
        }
    });
    
    return this;
}