var GEOM = GEOM || {};
GEOM.EARTH_RAD_MI = 3959;
GEOM.MI_PER_U = GEOM.EARTH_RAD_MI;
GEOM.KM_TO_MI = 0.621371192;
GEOM.AU_TO_MI = 92955807.3;
GEOM.SUN_RAD_MI = 432450;
GEOM.MOON_RAD_MI = 1079.6;

GEOM.lookAt = function(origin,point){
    var coord=GLGE.subVec3(origin, point);
    var zvec=GLGE.toUnitVec3(coord);
    var xvec=GLGE.toUnitVec3(GLGE.crossVec3([0,0,1],zvec));
    var yvec=GLGE.toUnitVec3(GLGE.crossVec3(zvec,xvec));		
    var rot =  [xvec[0], yvec[0], zvec[0], 0,
                xvec[1], yvec[1], zvec[1], 0,
                xvec[2], yvec[2], zvec[2], 0,
                0,       0,       0,       1];
    return rot;
}

GEOM.genGrid = function() {
    //draw grid
	var positions=[];
	for(var x=-50; x<50;x++){
		if(x!=0){
			positions.push(x);positions.push(0);positions.push(-50);
			positions.push(x);positions.push(0);positions.push(50);
			positions.push(50);positions.push(0);positions.push(x);
			positions.push(-50);positions.push(0);positions.push(x);
		}
	}
	    
	var line=(new GLGE.Object).setDrawType(GLGE.DRAW_LINES);
	line.setMesh((new GLGE.Mesh).setPositions(positions));
	line.setMaterial(XMLdoc.getElement( "lines" ));
    return line;
}

GEOM.genPlanet = function(rad_sm, res, mat, scene) {
    //Todo: this is a code chunk. Need to fix it up before use
    var spr = new GLGE.Sphere;
    spr.setRadius(rad_sm/GEOM.MI_PER_U);
    spr.setHorizontal(res);
    spr.setVertical(res);
    var sphere= new GLGE.Object;
	sphere.setMesh(spr);
    sphere.setMaterial(XMLdoc.getElement(mat));
    scene.addObject(sphere);
    return sphere
}

GEOM.spr2XYZ = function(up, right, mag) {
    //Convert spherical coordinates to XYZ. Angles in radians
    var d = [mag * Math.cos(up) * Math.cos(right), 
             mag * Math.cos(up) * Math.sin(right),
             mag * Math.sin(up)];
    return d;
}

GEOM.geo2XYZ = function(lat_deg, lon_deg, alt_sm) {
    //Convert geographic coordinates (latitude[deg], longitude[deg], altitude[sm])
    //to global XYZ coordinates
    var alt = (alt_sm + GEOM.EARTH_RAD_MI) / GEOM.MI_PER_U;
    var lat = lat_deg * Math.PI/180;
    var lon = lon_deg * Math.PI/180;
    return GEOM.spr2XYZ(lat, lon, alt);
}