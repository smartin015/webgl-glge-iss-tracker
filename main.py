#!/usr/bin/env python
#
# Copyright 2007 Google Inc.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
import cgi
import webapp2
import os
from google.appengine.ext.webapp import template
from google.appengine.api import urlfetch

class MainPage(webapp2.RequestHandler):
  def get(self):
    url = "http://celestrak.com/NORAD/elements/stations.txt"
    result = urlfetch.fetch(url)
    if result.status_code == 200:
      path = os.path.join(os.path.dirname(__file__), 'templates', 'index.html')
      self.response.out.write(template.render(path, {"tle": result.content}))
    else:
      self.response.out.write("Error fetching TLE")

app = webapp2.WSGIApplication([
  ('/', MainPage)
], debug=True)
